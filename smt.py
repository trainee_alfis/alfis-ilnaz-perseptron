import numpy as np
import os
import random
from PIL import Image
from functools import reduce
import sys



def is_success(num: int) -> bool:
    """функция делает вывод, т.е. решает правильное число или нет
    
    Arguments:
        num {int} -- сумма произведений входов на веса
    
    Returns:
        bool -- True, если число правильное, если не правильное - False
    """
    return True if num > 0.5 else False


def add_negative(array: np.array) -> np.array:
    """функция делает некоторые элементы массива отрицательными
    
    Arguments:
        array {np.array} -- массив, некоторые элементы которого нужно сделать отрицательными
    
    Returns:
        np.array -- массив numpy
    """
    n, m = array.shape

    for i in range(n):
        for j in range(m):
            array[i][j] *= -1 if j % 2 == 0 else 1

    counter1, counter2 = 0, 0

    for i in range(n):
        for j in range(m):
            if array[i][j] > 0:
                counter1 += 1
            else:
                counter2 += 1
    
    print("положительных: " + str(counter1) + " отрицательных: " + str(counter2))
    
    return array


# def convert_to_matrix(*filenames: str):
#     array_list = [(np.array(Image.open(directory + filename).resize((28, 28)).convert("L")) / 255).ravel() for filename, directory in zip(filenames, ['Circle/', 'Rectangle/', 'Triangle/'])]
#     return reduce(lambda a, b: np.column_stack((a, b)), array_list)


def convert_to_matrix(filename: str, directory: str) -> np.array:
    """функция конвертирует изображение в вектор-столбец цветов
    
    Arguments:
        filename {str} -- название файла изображения
        directory {str} -- директория, в которой он находится относительно этого скрипта
    
    Returns:
        np.array -- вектор-столбец цветов
    """
    return (np.array(Image.open(directory + filename).resize((28, 28)).convert("L")) / 255).ravel().reshape(-1)


stat = {'success': 0, 'total': 0}
history_of_rate = {'old': 0, 'new' : 0}

neurons = add_negative(np.random.sample((3, 28 ** 2)))
alpha = 0.1
step = 0.0005


while True:
    for figures in zip(os.listdir('Circle/'), os.listdir('Rectangle/'), os.listdir('Triangle/')):
        if len(list(filter(lambda value: type(value), figures))) < 3:
            break

        for img, i, directory in zip(figures, range(3), ['Circle/', 'Rectangle/', 'Triangle/']):
            stat['total'] += 1
 
            img_matrix = convert_to_matrix(img, directory)
            multiplication_result = neurons[i,:] @ img_matrix

            if is_success(multiplication_result):
                neurons[i,:] += img_matrix * alpha
                stat['success'] += 1
            else:
                neurons[i,:] -= img_matrix * alpha

            accuracy = stat['success'] / stat['total']
            history_of_rate['new'] = accuracy


            if history_of_rate['new'] < history_of_rate['old'] and stat['total'] % 1250 == 0:
                step *= -1
                print(f"точность: {stat['success']} на {stat['total']}, точность: {accuracy * 100}%", end='\r')
                # print(f"точность: {accuracy * 100}%", end='\r')
        

            alpha += step
            history_of_rate['old'] = accuracy


        # multiplication_result = neurons @ imgs_matrix

        # n, m = multiplication_result.shape

        # for i in range(n):
        #     for j in range(m):
        #         if i == j and is_success(multiplication_result[i][j]):
        #             neurons[i,:] += (imgs_matrix[:, j] * alpha)
        #             stat['success'] += 1
        #             stat['total'] += 1
        #         elif i == j:
        #             neurons[i,:] -= (imgs_matrix[:, j] * alpha)
        #             stat['total'] += 1
                

        #         accuracy = stat['success'] / stat['total']
        #         history_of_rate['new'] = accuracy


        #         if history_of_rate['new'] < history_of_rate['old'] and stat['total'] % 1250 == 0:
        #             step *= -1
        #             print(f"точность: {stat['success']} на {stat['total']}, точность: {accuracy * 100}%", end='\r')
        #             # print(f"точность: {accuracy * 100}%", end='\r')
                

        #         alpha += step
        #         history_of_rate['old'] = accuracy